function buildTicket(ticketData, cardHash, drawHistory = []) {
    const ticket = document.createElement('div')
    ticket.classList.add('ticket')
    ticket.setAttribute('ticket_hash', ticketData._hash)
    ticket.setAttribute('card_hash', cardHash)
    ticket.id = `ticket_${ticketData._id}`
    const label = document.createElement('h2')
    label.classList.add('label')
    label.innerText = `Ticket ${ticketData._id}`
    if (typeof requestCheck !== 'undefined') {
        label.onclick = requestCheck
    }
    ticket.append(label)

    ticketData._numbers.forEach((row, rowId) => {
        row.forEach((cell, colId) => {
            ticket.appendChild(buildCell(colId, rowId, cell, drawHistory))
        })
    })
    console.log(drawHistory)

    return ticket
}

function buildCell(colId, rowId, value, drawHistory = []) {
    const isDabbable = value != 0
    const cell = document.createElement('div')
    cell.id = `number_${rowId}_${colId}`
    cell.classList.add('number')
    if (isDabbable) {
        cell.classList.add('dabbable')
        if (drawHistory.includes(value)) {
            cell.classList.add('dabbed')
        }
        cell.innerText = value
        if (typeof dabCell !== 'undefined') {
            cell.onclick = dabCell
        }
    }

    return cell
}
