const socket = io()

const root = document.getElementById('root')

socket.emit('new-user', { roomName: roomName, code: code })

socket.on('system', data => {
    alert(data.msg)
    location.href = data.redirect
})

socket.on('game-card', data => {
    if (data.roomName !== roomName) {
        return
    }
    root.innerText = ''
    root.appendChild(buildStrip(data.card))
})


function buildStrip(card) {
    const strip = document.createElement('div')
    strip.classList.add('strip')
    const header = document.createElement('h1')
    header.innerText = 'Bingo'
    strip.appendChild(header)

    card._tickets.forEach(ticketData => {
        strip.appendChild(buildTicket(ticketData, card._hash))
    });

    const hashElement = document.createElement('div')
    hashElement.classList.add('hash')
    hashElement.innerText = card._hash
    strip.appendChild(hashElement)

    return strip
}

function requestCheck(event) {
    const ticket = event.target.parentNode
    const cardHash = ticket.getAttribute('card_hash')
    const ticketHash = ticket.getAttribute('ticket_hash')
    socket.emit('ticket-check', {
        roomName: roomName,
        code: code,
        cardHash: cardHash,
        ticketHash: ticketHash
    })
}

function dabCell(event) {
    const target = event.target
    target.classList.toggle('dabbed')
}
