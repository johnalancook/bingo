const socket = io()

const nextBallButton = document.getElementById('next-ball')
const newGameButton = document.getElementById('new-game')

const ball = document.getElementById('ball')
const call = document.getElementById('call')

const checking = document.getElementById('checking')
const playerStatus = document.getElementById('player-count')

const callList = [
    '',
    'Kelly’s Eye',
    'One Little Duck',
    'Cup of Tea',
    'Knock at the Door',
    'Man Alive',
    'Tom Mix',
    'Lucky Seven',
    'Garden Gate',
    'Doctor’s Orders',
    'Boris’s Den',
    'Legs 11',
    'One Dozen',
    'Unlucky for Some',
    'Valentine’s Day',
    'Young and Keen',
    'Sweet 16',
    'Dancing Queen',
    'Coming of Age',
    'Goodbye Teens',
    'One Score',
    'Royal Salute',
    'Two Little Ducks',
    'Thee and Me',
    'Two Dozen',
    'Duck and Dive',
    'Pick and Mix',
    'Gateway to Heaven',
    'Over Weight',
    'Rise and Shine',
    'Dirty Gertie',
    'Get Up and Run',
    'Buckle My Shoe',
    'Dirty Knee',
    'Ask for More',
    'Jump and Jive',
    'Three Dozen',
    'More than 11',
    'Christmas Cake',
    'Steps',
    'Naughty 40',
    'Time for Fun',
    'Winnie the Pooh',
    'Down on Your Knees',
    'Droopy Drawers',
    'Halfway There',
    'Up to Tricks',
    'Four and Seven',
    'Four Dozen',
    'PC',
    'Half a Century',
    'Tweak of the Thumb',
    'Danny La Rue',
    'Stuck in the Tree',
    'Clean the Floor',
    'Snakes Alive',
    'Was She Worth It?',
    'Heinz Varieties',
    'Make Them Wait',
    'Brighton Line',
    'Five Dozen',
    'Bakers Bun',
    'Turn the Screw',
    'Tickle Me 63',
    'Red Raw',
    'Old Age Pension',
    'Clickety Click',
    'Made in Heaven',
    'Saving Grace',
    'Either Way Up',
    'Three Score and 10',
    'Bang on the Drum',
    'Six Dozen',
    'Queen B',
    'Candy Store',
    'Strive and Strive',
    'Trombones',
    'Sunset Strip',
    'Heaven’s Gate',
    'One More Time',
    'Eight and Blank',
    'Stop and Run',
    'Straight On Through',
    'Time for Tea',
    'Seven Dozen',
    'Staying Alive',
    'Between the Sticks',
    'Torquay in Devon',
    'Two Fat Ladies',
    'Nearly There',
    'Top of the Shop'
]

var drawHistory = []

socket.emit('new-drawer', { roomName: roomName, drawerCode: drawerCode })

socket.on('system', systemsData => {
    alert(systemsData.msg)
    location.href = systemsData.redirect
})

socket.on('users-updated', userData => {
    if (userData.roomName !== roomName) {
        return
    }
    playerStatus.innerHTML = `${userData.players} Players | ${userData.callers} Callers`
})

socket.on('draw-next', drawData => {
    if (drawData.roomName !== roomName) {
        return
    }
    setDraw(drawData.draw)
    setDrawHistory(drawData.drawHistory)
    if (drawData.msg) {
        alert(drawData.msg)
    }
    checking.innerText = ''
})

socket.on('new-game', roomName => {
    setDraw(0)
    clearDrawHistory();
    checking.innerText = ''
})

socket.on('ticket-check', ticketData => {
    if (ticketData.roomName !== roomName) {
        return
    }
    addTicketToCheck(ticketData)
})

nextBallButton.addEventListener('click', () => {
    socket.emit('draw-request', { roomName: roomName, drawerCode: drawerCode })
})

newGameButton.addEventListener('click', () => {
    socket.emit('new-game-request', { roomName: roomName, drawerCode: drawerCode })
})

function setDraw(ballNumber) {
    ball.innerText = ballNumber == 0 ? '..' : ballNumber
    call.innerText = callList[ballNumber]
}

function setDrawHistory(drawnBalls) {
    drawHistory = drawnBalls
    drawnBalls.forEach(element => {
        document.getElementById('cell' + element).classList.add('selected')
    });
}

function clearDrawHistory() {
    const elements = document.getElementsByClassName('cell selected')
    while (elements.length > 0) {
        elements[0].classList.remove('selected')
    }
}

function addTicketToCheck(ticketData) {
    removeTicket(document.getElementById(`ticket-${ticketData.strip}-${ticketData.ticket._hash}`))

    const strip = document.createElement('div')
    strip.id = `ticket-${ticketData.strip}-${ticketData.ticket._hash}`
    strip.classList.add('strip')

    const hashElement = document.createElement('div')
    hashElement.classList.add('hash')
    hashElement.innerText = ticketData.strip
    strip.appendChild(hashElement)

    strip.appendChild(buildTicket(ticketData.ticket, ticketData.strip, drawHistory))

    const closeBtn = document.createElement('button')
    closeBtn.innerText = 'X'
    closeBtn.onclick = closeStrip
    strip.appendChild(closeBtn)

    checking.appendChild(strip)
}

function closeStrip(event) {
    removeTicket(event.target.parentNode)
}

function removeTicket(ticketElement) {
    if (ticketElement) {
        ticketElement.parentNode.removeChild(ticketElement)
    }
}
