const crypto = require('crypto')
const shuffle = require('./shuffle')
const range = require('./range')
const Strip = require('./Strip')

class Room {
    constructor(name) {
        const [code, drawerCode] = crypto.createHash('md5').update(name).update(Date.now().toString()).digest('hex').match(/.{1,16}/g)

        this._name = name
        this._code = code
        this._drawerCode = drawerCode
        this._callers = {}
        this._players = {}
        this._strips = {}
        this._tickets = []
        this._toDraw = []
        this._drawn = 0
        this._drawHistory = []
    }

    get name() {
        return this._name
    }

    get callers() {
        return this._callers
    }

    get players() {
        return this._players
    }

    get lastDraw() {
        return this._drawn
    }

    get drawHistory() {
        return this._drawHistory
    }

    newGame() {
        this._tickets = []
        this._strips = []
        this._toDraw = shuffle(range(1, 90))
        this._drawn = 0
        this._drawHistory = []
    }

    addCaller(callerId, caller) {
        this._callers[callerId] = caller
    }

    addPlayer(playerId, player) {
        this._players[playerId] = player
    }

    removeUser(userId) {
        if (this._players[userId]) delete this._players[userId]
        if (this._callers[userId]) delete this._callers[userId]
    }

    hasUser(userId) {
        return (this._players[userId]) || (this._callers[userId])
    }

    getUserCount() {
        return {
            roomName: this._name,
            callers: Object.keys(this._callers).length,
            players: Object.keys(this._players).length
        }
    }

    newStrip() {
        const strip = new Strip(this)
        this._strips[strip.hash] = strip
        return strip
    }

    hasTicket(ticketHash) {
        return this._tickets.includes(ticketHash)
    }

    addTicket(ticket) {
        if (!this.hasTicket(ticket.hash)) {
            this._tickets.push(ticket.hash)
        }
    }

    addStrip(strip) {
        this._strips[strip.hash] = strip
    }

    drawNext() {
        if (this._toDraw.length === 0) {
            this._drawn = 0
        } else {
            this._drawn = this._toDraw.pop()
            this._drawHistory.push(this.lastDraw)
            this._drawHistory = this._drawHistory.sort((a, b) => a - b)
        }
        return this.drawCurrent()
    }

    drawCurrent() {
        const ret = {
            roomName: this.name,
            draw: this.lastDraw,
            drawHistory: this.drawHistory
        }
        if (this._toDraw.length === 0) {
            ret.msg = "All balls drawn."
        }
        return ret
    }

}

module.exports = Room
