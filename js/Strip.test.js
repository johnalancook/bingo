const Strip = require('./Strip')
const Room = require('./Room')
const Chance = require('chance')
const chance = new Chance('random seed')
const range = require('./range')

test('Constructor', () => {
    const mathRandomSpy = jest.spyOn(Math, 'random')
    mathRandomSpy.mockImplementation(() => chance.floating({ min: 0, max: 1, fixed: 15 }))

    const strip = new Strip(new Room(chance.string()))
    expect(strip.hash).toBe('00a0a6a26a16bd10a3a5917b533e0b44')
    expect(strip.tickets[0].hash).toBe('53b52f04079c5eaea6b53d7de0222426')

    mathRandomSpy.mockRestore()
})

test('Check duplicate tickets', () => {
    const room = new Room(chance.string())
    const chance1 = new Chance('random seed')
    const chance2 = new Chance('random seed')
    {
        const mathRandomSpy = jest.spyOn(Math, 'random')
        mathRandomSpy.mockImplementation(() => chance1.floating({ min: 0, max: 1, fixed: 15 }))

        const strip = new Strip(room)
        expect(strip.hash).toBe('f8fe4d2c10b5bcdb399c3341ff9cbab6')

        mathRandomSpy.mockRestore()
    }
    {
        const mathRandomSpy = jest.spyOn(Math, 'random')
        mathRandomSpy.mockImplementation(() => chance2.floating({ min: 0, max: 1, fixed: 15 }))

        const strip = new Strip(room)
        expect(strip.hash).not.toBe('f8fe4d2c10b5bcdb399c3341ff9cbab6')

        mathRandomSpy.mockRestore()
    }
})

describe('Check contraints', () => {
    const seed = process.env.SEED || (new Chance()).hash();
    var chance;
    var mathRandomSpy;

    var strip;

    beforeAll(() => {
        console.log(`Check strip constraints seed: ${seed}`)
        chance = new Chance(seed)
        mathRandomSpy = jest.spyOn(Math, 'random')
        mathRandomSpy.mockImplementation(() => chance.floating({ min: 0, max: 1, fixed: 15 }))

        strip = new Strip(new Room(chance.string()))

        mathRandomSpy.mockRestore()
    })

    test('There are 6 tickets per strip', () => {
        expect(strip.tickets.length).toBe(6)
    })

    test('Each ticket is 9 cells wide by 3 cells deep.', () => {
        strip.tickets.forEach(ticket => {
            expect(ticket.numbers.length).toBe(3);
            ticket.numbers.forEach(row => {
                expect(row.length).toBe(9)
            })
        })
    })

    test('Every row must have exactly 5 numbers in it', () => {
        strip.tickets.forEach(ticket => {
            ticket.numbers.forEach(row => {
                expect(row.filter((v) => v > 0).length).toBe(5)
            })
        })
    })

    test('The 3 cell high column in each ticket must have between 1-3 numbers in it but never zero numbers.', () => {
        strip.tickets.forEach(ticket => {
            for (var i = 0; i < ticket.numbers[0].length; i++) {
                const columnNumbers = getColumn(ticket.numbers, i)
                expect(columnNumbers.length).toBeGreaterThanOrEqual(1)
                expect(columnNumbers.length).toBeLessThanOrEqual(3)
            }
        })
    })

    test('In each 3 cell high column, the numbers are sorted with lower numbers higher than larger numbers.', () => {
        strip.tickets.forEach(ticket => {
            for (var i = 0; i < ticket.numbers[0].length; i++) {
                const columnNumbers = getColumn(ticket.numbers, i)
                if (columnNumbers.length > 1) {
                    for (var pos = 1; pos < columnNumbers.length; pos++) {
                        expect(columnNumbers[pos]).toBeGreaterThan(columnNumbers[pos - 1])
                    }
                }
            }
        })
    })

    test('All the numbers 1 to 90 are used only once in each set of 6 tickets.', () => {
        const numbersInStrip = strip.tickets.reduce((carry, ticket) => {
            return carry.concat(
                ticket.numbers.reduce((carry, row) => {
                    return carry.concat(row.filter(val => val > 0))
                }, [])
            )
        }, [])
        expect(numbersInStrip.length).toBe(90)
        expect(numbersInStrip).toEqual(expect.arrayContaining(range(1, 90)))
    })
})

function getColumn(numbers, columnId) {
    return numbers.reduce((carry, row) => {
        if (row[columnId] > 0) {
            carry.push(row[columnId])
        }
        return carry
    }, [])
}
