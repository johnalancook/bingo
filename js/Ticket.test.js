const Ticket = require('./Ticket')
const Chance = require('chance')
const chance = new Chance()

test('Ticket constructor.', () => {
    const id = chance.character()
    const hash = chance.hash()
    const numbers = chance.string()

    const ticket = new Ticket(id, hash, numbers)

    expect(ticket.id).toBe(id)
    expect(ticket.hash).toBe(hash)
    expect(ticket.numbers).toBe(numbers)
})
