class Ticket {
    constructor(id, hash, numbers) {
        this._id = id
        this._hash = hash
        this._numbers = numbers
    }

    get id() {
        return this._id
    }

    get hash() {
        return this._hash
    }

    get numbers() {
        return this._numbers
    }
}

module.exports = Ticket
