const Room = require('./Room')
const Strip = require('./Strip')
const Ticket = require('./Ticket')
const Chance = require('chance')
const chance = new Chance()

test('Constructor', () => {
    const name = chance.string()
    const room = new Room(name)
    expect(room.name).toBe(name)
    expect(Object.keys(room.callers).length).toBe(0)
    expect(Object.keys(room.players).length).toBe(0)
    expect(room.lastDraw).toBe(0)
    expect(room.drawHistory.length).toBe(0)
})

test('New game', () => {
    const room = new Room(chance.string())
    room.newGame()

    expect(Object.keys(room._tickets).length).toBe(0)
    expect(room._toDraw.length).toBe(90)
    expect(room.lastDraw).toBe(0)
    expect(room.drawHistory.length).toBe(0)
})

test('Adding and removing users', () => {
    const name = chance.string()
    const room = new Room(name)

    expect(room.getUserCount().roomName).toBe(name)
    expect(room.getUserCount().callers).toBe(0)
    expect(room.getUserCount().players).toBe(0)

    room.addCaller(6, chance.string())
    expect(room.getUserCount().callers).toBe(1)
    expect(room.getUserCount().players).toBe(0)

    room.addPlayer(8, chance.string())
    expect(room.getUserCount().callers).toBe(1)
    expect(room.getUserCount().players).toBe(1)

    expect(room.hasUser(6)).toBeTruthy()
    expect(room.hasUser(8)).toBeTruthy()
    expect(room.hasUser(2)).toBeFalsy()

    room.removeUser(2)
    expect(room.getUserCount().callers).toBe(1)
    expect(room.getUserCount().players).toBe(1)

    room.removeUser(6)
    expect(room.getUserCount().callers).toBe(0)
    expect(room.getUserCount().players).toBe(1)

    room.removeUser(8)
    expect(room.getUserCount().callers).toBe(0)
    expect(room.getUserCount().players).toBe(0)
})

test('Adding ticket', () => {
    const room = new Room(chance.string())
    room.newGame()
    const ticket = new Ticket(chance.character(), chance.hash(), chance.string())
    const ticket2 = new Ticket(chance.character(), ticket.hash, chance.string())

    expect(room._tickets.length).toBe(0)

    room.addTicket(ticket)

    expect(room._tickets.length).toBe(1)
    expect(room._tickets).toEqual(expect.arrayContaining([ticket.hash]))

    room.addTicket(ticket2)
    expect(room._tickets.length).toBe(1)
    expect(room._tickets).toEqual(expect.arrayContaining([ticket.hash]))
    expect(room._tickets).toEqual(expect.arrayContaining([ticket2.hash]))
})

test('Create a new strip', () => {
    const room = new Room(chance.string())
    room.newGame()
    const strip = room.newStrip()
    expect(strip).toBeInstanceOf(Strip)
    expect(room._strips[strip.hash]).toBeInstanceOf(Strip)
    expect(room._strips[strip.hash]).toBe(strip)
})

test('Drawing a ball.', () => {
    const roomName = chance.string()
    const room = new Room(roomName)
    room.newGame()

    for (var i = 0; i < 10; i++) {
        const draw = room.drawNext()
        expect(draw.roomName).toBe(roomName)
        expect(draw.draw).toBe(room.lastDraw)
        expect(draw.drawHistory).toEqual(expect.arrayContaining([draw.draw]))
        expect(draw.drawHistory).toEqual(room.drawHistory)
    }

})

test('No balls left when drawing.', () => {
    const roomName = chance.string()
    const room = new Room(roomName)

    const draw = room.drawNext()
    expect(draw.roomName).toBe(roomName)
    expect(draw.draw).toBe(0)
    expect(draw.drawHistory).toEqual(room.drawHistory)
    expect(draw.msg).toBe('All balls drawn.')
})
