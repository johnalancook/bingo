function range(min, max) {
    if (min > max) {
        [min, max] = [max, min]
    }
    return Array.from({ length: max - min + 1 }, (_, i) => i + min)
}

module.exports = range
