const range = require('./range')

test.each([
    [0, 3, [0, 1, 2, 3]],
    [8, 11, [8, 9, 10, 11]],
    [-2, 2, [-2, -1, 0, 1, 2]],
    [3, 0, [0, 1, 2, 3]],
])('Create a range between %i and %i', (min, max, expected) => {
    expect(range(min, max)).toEqual(expected)
})
