const rndInteger = require('./rndInteger')

function shuffle(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = rndInteger(0, i)
        var temp = array[i]
        array[i] = array[j]
        array[j] = temp
    }
    return array
}

module.exports = shuffle
