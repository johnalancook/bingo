const getRange = require('./range')
const shuffle = require('./shuffle')
const rndInteger = require('./rndInteger')
const crypto = require('crypto')
const Ticket = require('./Ticket')

class Strip {
    constructor(room) {
        const set = createValidSet(room)

        this._tickets = []

        const hashFactory = crypto.createHash('md5')

        for (var i = 1; i <= 6; i++) {
            const positionedTicket = positionTicketNumbers(set[i], i)
            room.addTicket(positionedTicket)
            this._tickets.push(positionedTicket)
            hashFactory.update(positionedTicket.hash)
        }
        this._hash = hashFactory.digest('hex')

        room.addStrip(this)
    }

    get tickets() {
        return this._tickets
    }

    get hash() {
        return this._hash
    }
}

function createValidSet(room, iteration = 0) {
    const set = createNumberSet()
    if (setIsValid(set, room) || iteration > 10) {
        return set
    } else {
        return createValidSet(room, iteration + 1)
    }
}

function setIsValid(set, room) {
    for (const ticket of set) {
        if (ticket) {
            if (room.hasTicket(createTicketHash(ticket))) {
                return false;
            }
        }
    }
    return true
}

function createTicketHash(ticket) {
    return crypto.createHash('md5').update(ticket.toString()).digest('hex')
}

function createNumberSet() {
    const tickets = []
    const numbers = []
    for (var i = 1; i <= 6; i++) {
        tickets[i] = [];
        for (var j = 0; j < 9; j++) {
            tickets[i][j] = [];
        }
    }

    for (i = 1; i < 90; i+=10) {
        var range = shuffle(getRange(i, i+9));
        numbers[(i - 1) / 10] = range;
    }

    for (j = 0; j < 9; j++) {
        for (i = 1; i <= 6; i++) {
          tickets[i][j].push(numbers[j].pop());
        }
    }

    for (i = 0; i < 4; i++) {
        const max = i === 3 ? 3 : 2;
        for (j = 0; j < 9; j++) {
            var tid;
            do {
                tid = rndInteger(1, 6);
            } while (
                tickets[tid][j].length >= max |
                tickets[tid].reduce((a, b) => { return a + b.length; }, 0) >= 15
            );
            tickets[tid][j].push(numbers[j].pop());
        }
    }

    for (i = 1; i <= 6; i++) {
        for (j = 0; j < 9; j++) {
            tickets[i][j] = tickets[i][j].sort((a, b) => a - b);
        }
    }

    return tickets
}

function positionTicketNumbers(ticket, i) {
    const grid = [[],[],[]];
    const hash = createTicketHash(ticket)
    for (var row = 0; row < 3; row ++) {
      grid[row] = [0,0,0,0,0,0,0,0,0];
      var valid_columns = shuffle(ticket.map((value, index) => value.length > 0 ? index : null).filter((value) => value !== null))
      var must_use = ticket.map((value, index) => value.length === 3 - row ? index : null).filter((value) => value !== null)
      must_use.forEach((col) => {
        valid_columns = valid_columns.filter((value) => value !== col)
        grid[row][col] = ticket[col].shift();
      });
      for (j = must_use.length; j < 5; j++) {
        var col = valid_columns.pop();
        grid[row][col] = ticket[col].shift();
      }
    }

    return new Ticket(String.fromCharCode(64 + i), hash, grid)
}

module.exports = Strip
