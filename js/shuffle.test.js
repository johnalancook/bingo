const shuffle = require('./shuffle')

test('Tests shuffling', () => {
    const data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    const shuffled = shuffle([...data])
    expect(shuffled.length).toBe(data.length)
    expect(shuffled).not.toEqual(data)
    expect(shuffled).toEqual(expect.arrayContaining(data))
})