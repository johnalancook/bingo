const rndInteger = require('./rndInteger')

test.each([
    [0, 10],
    [8, 16],
    [-10, -6],
    [-4, 4]
])('Test random integer between %i and %i', (min, max) => {
    var minRnd = max
    var maxRnd = min
    for (var i = 0; i < 100; i++) {
        const rnd = rndInteger(min, max)
        if (rnd < minRnd) minRnd = rnd
        if (rnd > maxRnd) maxRnd = rnd
        expect(rnd).toBeGreaterThanOrEqual(min)
        expect(rnd).toBeLessThanOrEqual(max)
    }
    expect(minRnd).toBe(min)
    expect(maxRnd).toBe(max)
})

test.each([
    [0, 10],
    [8, 16],
    [-10, -6],
    [-4, 4]
])('Test random integer between %i and %i (inverted input)', (min, max) => {
    var minRnd = max
    var maxRnd = min
    for (var i = 0; i < 100; i++) {
        const rnd = rndInteger(max, min)
        if (rnd < minRnd) minRnd = rnd
        if (rnd > maxRnd) maxRnd = rnd
        expect(rnd).toBeGreaterThanOrEqual(min)
        expect(rnd).toBeLessThanOrEqual(max)
    }
    expect(minRnd).toBe(min)
    expect(maxRnd).toBe(max)
})
