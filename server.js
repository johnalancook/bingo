try {
    require('dotenv').config()
} catch (ex) {}
const express = require('express')
const app = express()
const server = require('http').createServer(app)
const io = require('socket.io')(server)
const crypto = require('crypto')

const getRange = require('./js/range')
const rndInteger = require('./js/rndInteger')
const shuffle = require('./js/shuffle')
const Room = require('./js/Room')
const Strip = require('./js/Strip')

app.set('views', './views')
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(express.urlencoded({ extended: true }))
app.enable('trust proxy')

const rooms = {}

app.get('/', (req, res) => {
    res.render('index')
})

app.post('/room', (req, res) => {
    if (rooms[req.body.room] != null) {
        return res.redirect('/');
    }
    room = new Room(req.body.room)
    room.newGame()
    rooms[room.name] = room
    res.redirect(`/drawer/${room.name}/${room._drawerCode}`)
})

app.get('/room/:room/:code', (req, res) => {
    room = rooms[req.params.room]
    if (room == null || room._code !== req.params.code) {
        return res.redirect('/')
    }
    res.render('room', { room: room })
})

app.get('/drawer/:room/:drawerCode', (req, res) => {
    room = rooms[req.params.room]
    if (room == null || room._drawerCode !== req.params.drawerCode) {
        return res.redirect('/')
    }
    res.render('drawer', { room: room, host: `${req.protocol}://${req.headers.host}` })
})


io.on('connection', socket => {
    socket.on('new-drawer', (roomData) => {
        if (room = getDrawerRoom(roomData)) {
            socket.join(room.name)
            socket.join(`${room.name}-${room._drawerCode}`)
            room.addCaller(socket.id, socket)
            io.to(room.name).emit('users-updated', room.getUserCount())
        }
    })

    socket.on('new-user', (roomData) => {
        if (room = getRoom(roomData)) {
            socket.join(room.name)
            room.addPlayer(socket.id, socket)
            io.to(room.name).emit('users-updated', room.getUserCount())
        }
    })

    socket.on('draw-request', (roomData) => {
        if (room = getDrawerRoom(roomData)) {
            io.to(`${room.name}-${room._drawerCode}`).emit('draw-next', room.drawNext())
        }
    })

    socket.on('new-game-request', (roomData) => {
        if (room = getDrawerRoom(roomData)) {
            room.newGame()
            // Todo: notify drawers.
            io.to(room.name).emit('new-game', room.name)
            // Todo: send card to players
            Object.entries(room.players).forEach(([key, player]) => player.emit('game-card', {roomName: room.name, card: new Strip(room)}))
        }
    })

    socket.on('ticket-check', (ticketData) => {
        if ((room = getRoom(ticketData)) && (strip = room._strips[ticketData.cardHash])) {
            strip.tickets.forEach((ticket) => {
                if (ticket.hash == ticketData.ticketHash) {
                    io.to(`${room.name}-${room._drawerCode}`).emit('ticket-check', {
                        roomName: room.name,
                        strip: strip.hash,
                        ticket: ticket
                    })
                }
            })
        }
    })

    socket.on('disconnect', () => {
        const id = socket.id
        getUserRooms(socket).forEach(roomName => {
            room = rooms[roomName]
            if (room) {
                room.removeUser(id)
                io.to(room.name).emit('users-updated', room.getUserCount())
                if (room.getUserCount().callers === 0) {
                    io.to(room.name).emit('system', { msg: 'The room has closed.', redirect: '/' })
                    delete rooms[room.name]
                }
            }
        });
    })
})

server.listen(process.env.PORT, () => console.log('Server Started'))

function getRoom(roomData) {
    room = rooms[roomData.roomName]
    if (!room || room._code !== roomData.code) {
        return false
    }
    return room
}

function getDrawerRoom(roomData) {
    room = rooms[roomData.roomName]
    if (!room || room._drawerCode !== roomData.drawerCode) {
        return false
    }
    return room
}

function getUserRooms(socket) {
    const temp = Object.entries(rooms).reduce((names, [name, room]) => {
        if (!names.includes(name) && room.hasUser(socket.id)) {
            names.push(name)
        }
        return names
    }, [])
    return temp
}
